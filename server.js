
const  cors = require("cors");
const express =  require("express");
const bodyParser  = require("body-parser");
const app  = express();
const PORT  = process.env.PORT || 8080


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.json())


app.use('/api',require('./routes/api'));
app.listen(PORT,() =>
      

        console.log(`Server started on PORT  ${PORT}`)

);
