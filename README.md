# API-COVID

เป็น API ที่ ทำการ call API จากเว็บ   https://disease.sh/docs/**  อีกที เพื่อนำข้อมูลมาปรับเเต่งอีกที
อันนี้เป็นเว็บไซต์ที่เขียน ด้วย React ตาม  requirement  ผมได้เพิ่ม ฟังก์ชั่นบางอย่าง ไปคือ

1.แยกประเทศตามทวีป

2.ประเทศที่มีผู้ติดเชื้อมากที่สุด

เว็บไซต์ผมได้อัพขึ้น firebase host


https://covidweb-3c091.web.app

 

ส่วน API ที่ปรับแต่งข้อมูลแล้ว ผมได้อัพขึ้น heroku  

API ผมได้แบ่งออกเป็น  3 อย่าง

1. ข้อมูลผู้ติดเชื้อตามทวีป 

https://api-covid-v1.herokuapp.com/api/datacovid

รูปแบบ GET

Parameter : 

            date :  “12/8/20”

            continent : “Asia”
 

2.ข้อมูลผู้ติดเชื้อ 20 อันดับ

            https://api-covid-v1.herokuapp.com/api/datacovid-20

            รุปแบบ GET

Parameter : 

            date :  “12/8/20”

3.ดึงข้อมูลวันที่

            https://api-covid-v1.herokuapp.com/api/getdate

            รูปแบบ GET
