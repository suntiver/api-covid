const axios = require("axios");
const data = require("./Continent");


/* คิวรีข้อมูลจาก date ที่รับเข้ามา เเละเลือกเอา  20 อันดับที่ร้ายเเรง*/
exports.datacovid20  = async (req, res) => {

            const dataArray = [];
            const dataFor = [];
            const sum = {};
            let result;
            const result_data = [];

            await axios
                .get("https://disease.sh/v3/covid-19/historical")
                .then(function (response) {
                response.data.forEach((element) => {
                    dataArray.push(element);
                });
                });

            dataArray.map((item) => {
                const date = req.query.date;
                const cases = item.timeline.cases[date];
                const country = item.country;

                data.forEach((item) => {

                        dataFor.push([country, cases, date,item[1]]);

                })
            
            });


            for (var i = 0, c; (c = dataFor[i]); ++i) {
                //ตัวไม่ซ้ำ
                if (undefined === sum[c[0]]) {
                    sum[c[0]] = c;
                }
                //ตัวซ้ำ
                else {
                    sum[c[0]][1] += c[1];
                }
            }

            result = Object.keys(sum).map(function (val) {
                const country = sum[val][0];
                const dataCase = sum[val][1];
                const date = sum[val][2];
                const continent = sum[val][3];
                const setData = [country, dataCase, date,continent];
                return setData;
            });

            result.sort(function (a, b) {
                return b[1] - a[1];
            });


            for(var item  = 0 ;item<20;item++){
               
                    result_data.push({
                        country: result[item][0],
                        cases: result[item][1],
                        date: result[item][2],
                        continent:result[item][3]
                    });
            }


            return res.status(200).json({
                result_data,
            });
};





/* คิวรีข้อมูลจาก date ที่รับเข้ามา เเละ เลือก ทวีป*/

exports.datacovid = async (req, res) => {

        const dataArray = [];
        const dataFor = [];
        const sum = {};
        let result;
        const result_data = [];

        await axios
            .get("https://disease.sh/v3/covid-19/historical")
            .then(function (response) {
            response.data.forEach((element) => {
                dataArray.push(element);
            });
            });

        dataArray.map((item) => {
            const date = req.query.date;
            const cases = item.timeline.cases[date];
            const country = item.country;
            data.forEach((item) => {

                if (country == item[0]) {
                    if (item[1] == req.query.continent) {
                       dataFor.push([country, cases, date,item[1]]);
                    }
                }

            })
           
        });

    
        for (var i = 0, c; (c = dataFor[i]); ++i) {
            //ตัวไม่ซ้ำ
            if (undefined === sum[c[0]]) {
                sum[c[0]] = c;
            }
            //ตัวซ้ำ
            else {
                sum[c[0]][1] += c[1];
            }
        }

        result = Object.keys(sum).map(function (val) {
            const country = sum[val][0];
            const dataCase = sum[val][1];
            const date = sum[val][2];
            const continent = sum[val][3];
            const setData = [country, dataCase, date,continent];
            return setData;
        });

        result.sort(function (a, b) {
            return b[1] - a[1];
        });


    
        result.forEach(function (a) {

                result_data.push({
                    country: a[0],
                    cases: a[1],
                    date: a[2],
                    continent:a[3]
                
                });

        
        });
     


        return res.status(200).json({
            result_data,
        });
};





/* ดึงวันที่ตาม API ที่ให้มา  */

exports.getDate = async (req, res) => {

        var dataArray = [];
        var dataFor = [];
        await axios
            .get("https://disease.sh/v3/covid-19/historical")
            .then(function (response) {
            response.data.forEach((element) => {
                dataArray.push(element);
            });
            });

        dataArray.map((item) => {
            var date = req.query.date;
            var cases = item.timeline.cases[date];
            var country = item.country;

            dataFor.push([country, cases, date]);

        });

        const data = dataArray[0].timeline.cases
        const result_date  = Object.keys(data)
    
        return res.status(200).json({
            result_date
        });

}