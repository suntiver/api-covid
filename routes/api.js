const route             = require('express').Router();
const covidController  = require('../controllers/covidController');


route.get("/datacovid-20",covidController.datacovid20);
route.get("/datacovid",covidController.datacovid);
route.get("/getDate",covidController.getDate);


module.exports = route;